import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server extends Thread{

    public static void main(String[] args) throws IOException {
        Thread threadsCntrl = new Thread(new ThreadsControl());
        Thread Server = new Thread(new Server());
        threadsCntrl.start();
        Server.start();
    }

    @Override
    // contains the command panel of the server
    public void run() {
        System.out.println("Your server is running... Type to get statistics:\n" +
                "1- Number of connected clients\n" +
                "2- Number of Downloads per ressources");
        Scanner in = new Scanner(System.in);
        String option = "";
        while (true){
            option=in.next();
            switch (option){
                case "help":
                    System.out.println("Help **************** Type:\n" +
                            "1- Number of connected clients\n" +
                            "2- Number of Downloads per ressources");
                    break;
                case "1":
                    System.out.println(ThreadsControl.onlineClients+" are downloading now..");
                    break;
                case "2":
                    getDownNum();
                    break;
                default:
                    System.out.println("Type:\n1- Number of connected clients\n2- Number of Downloads per ressources");
            }
        }
    }

    // returns the number of downloads per ressource
    public void getDownNum(){
        String msg = "";
        int[] downTimes = ThreadsControl.downTimes;
        String[] res = ThreadsControl.resources;
        for (int i=0; i<downTimes.length; i++){
            msg += (i+1) + "- "+res[i]+ " ----> " + downTimes[i]+"\n";
        }
        System.out.println(msg);
    }
}
class ThreadsControl extends Thread{
    public static final int PORT=12345;
    final static String agreements = "Here are the terms of reference. Do you accept? yes or no";
    final static String[] resources= {"TeamViewer", "Mario-AIU", "Winrar", "VLC"};
    final static String[] urls = {
            "https://download.teamviewer.com/download/TeamViewer_Setup.exe",
            "https://github.com/mounir1/Mario-AIU/raw/master/Mario-AIU.exe",
            "http://win-rar.com/predownload.html?spV=true&subD=true&f=wrar540.exe",
            "http://get.videolan.org/vlc/2.2.5.1/win32/vlc-2.2.5.1-win32.exe"};

    static int[] downTimes= new int[urls.length];
    static int bufferSize;
    static int onlineClients;

    @Override
    public void run(){
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            bufferSize=serverSocket.getReceiveBufferSize();
            while (true){
                Socket socket = serverSocket.accept();
                new ServerThread(socket).start(); // open a new thread for the new client
            }
        }catch (IOException e){
            e.getMessage();
        }
    }

    static public void addClients(){
        onlineClients++;
    }
    static public void removeClients(){
        onlineClients--;
    }

}
