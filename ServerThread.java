import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread{
    private Socket connection;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    final String agreements = ThreadsControl.agreements;
    final String[] resources= ThreadsControl.resources;
    final String[] contents= ThreadsControl.urls;
    int[] downTimes= ThreadsControl.downTimes;

    ServerThread(Socket socket){
        this.connection=socket;
    }

    @Override
    public void run() {
        try{
            //Trying to connect and have conversation
            setupStreams();
            if(agreementAccepted()){
                ThreadsControl.addClients(); // increment the number of the connected clients
                cnxProtocol();
            }
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        } finally{
            ThreadsControl.removeClients(); // decrement the number of the connected clients
            closeConnection();
        }
    }

    //get stream to send and receive data
    private void setupStreams() throws IOException{
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        input = new ObjectInputStream(connection.getInputStream());
    }

    // Terms of reference
    private boolean agreementAccepted() throws ClassNotFoundException, IOException{
        try {
            sendMessage(agreements);
            String res = (String) input.readObject();
            if(res.equals("yes")){
                return true;
            }else{
                return false;
            }
        }catch (IOException e){}
        return false;
    }

    // cnx protocol
    private void cnxProtocol() throws IOException{
        sendMessage("What do you wanna download?");
        sendResources();
        int index=0;
        try{
            while (true) {
                String respond = (String) input.readObject();
                if (respond.equalsIgnoreCase("END")) break;
                try {
                    index = Integer.valueOf(respond);
                    sendMessage("You are downloading " + resources[index - 1]);
                    sendMessage(contents[index - 1]);
                    downTimes[index - 1]++;
                    sendMessage("Do you wanna download something else? Enter END for exit");
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    sendMessage("Invalid Index, Enter a valid one!!");
                    continue;
                }
            }
        }catch(ClassNotFoundException | IOException e){
        }
    }

    // closes the cnx
    public void closeConnection(){
        try{
            output.close(); //Closes the output path to the client
            input.close(); //Closes the input path to the server, from the client.
            connection.close(); //Closes the connection between you can the client
        }catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

    //Send a message to the client
    private void sendMessage(String message){
        try{
            output.writeObject(message);
            output.flush();
        }catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

    //send the ressources list to the client
    private void sendResources(){
        String txt ="";
        for (int i=0;i<resources.length;i++){
            txt = txt+ (i+1)+"- "+resources[i]+"\n\n";
        }
        sendMessage(txt);
    }


}