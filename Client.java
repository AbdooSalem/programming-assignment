import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Client extends javax.swing.JFrame {

    // GUI variables *************************************
    private javax.swing.JButton agreeBtn;
    private javax.swing.JPanel ControlPanel;
    private javax.swing.JButton disagreeBtn;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextArea termsArea;
    private javax.swing.JPanel welcomePanel;
    private javax.swing.JTextArea cmdArea;
    private javax.swing.JScrollPane cmdPane;
    private javax.swing.JButton exitBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField messageArea;
    private javax.swing.JTextArea resArea;
    private javax.swing.JScrollPane resPane;
    private javax.swing.JButton sendBtn;
    private javax.swing.JScrollPane termsPanel;

    // Connection variables *************************************

    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String message = "";
    private String serverIP;
    private Socket connection;

    public Client(String host) {
        serverIP = host;
        initComponents();
    }

    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        welcomePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        termsPanel = new javax.swing.JScrollPane();
        termsArea = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        disagreeBtn = new javax.swing.JButton();
        agreeBtn = new javax.swing.JButton();
        ControlPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        resPane = new javax.swing.JScrollPane();
        resArea = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        cmdPane = new javax.swing.JScrollPane();
        cmdArea = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        exitBtn = new javax.swing.JButton();
        messageArea = new javax.swing.JTextField();
        sendBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("gDownloader");
        setLocation(new java.awt.Point(0, 0));
        setLocationByPlatform(true);
        setResizable(false);
        setSize(new java.awt.Dimension(900, 800));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        jLabel1.setText("Terms of use:");

        termsArea.setEditable(false);
        termsArea.setColumns(20);
        termsArea.setRows(5);
        termsPanel.setViewportView(termsArea);

        cmdArea.setEditable(false);
        resArea.setEditable(false);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel2.setText("Software Downloader");

        disagreeBtn.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        disagreeBtn.setText("Disagree and Quit");
        disagreeBtn.setPreferredSize(new java.awt.Dimension(140, 30));
        disagreeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DisagreeBtnActionPerformed(evt);
            }
        });

        agreeBtn.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        agreeBtn.setText("Agree and Continue");
        agreeBtn.setToolTipText("");
        agreeBtn.setPreferredSize(new java.awt.Dimension(140, 30));
        agreeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgreeBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout WelcomePanelLayout = new javax.swing.GroupLayout(welcomePanel);
        welcomePanel.setLayout(WelcomePanelLayout);
        WelcomePanelLayout.setHorizontalGroup(
                WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(WelcomePanelLayout.createSequentialGroup()
                                .addGroup(WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(WelcomePanelLayout.createSequentialGroup()
                                                .addGap(209, 209, 209)
                                                .addComponent(jLabel2))
                                        .addGroup(WelcomePanelLayout.createSequentialGroup()
                                                .addGap(45, 45, 45)
                                                .addComponent(jLabel1)))
                                .addContainerGap(240, Short.MAX_VALUE))
                        .addGroup(WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(WelcomePanelLayout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addGroup(WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, WelcomePanelLayout.createSequentialGroup()
                                                        .addGap(28, 28, 28)
                                                        .addComponent(disagreeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(agreeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(23, 23, 23))
                                                .addComponent(termsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 568, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        WelcomePanelLayout.setVerticalGroup(
                WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(WelcomePanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel1)
                                .addGap(309, 309, 309))
                        .addGroup(WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(WelcomePanelLayout.createSequentialGroup()
                                        .addContainerGap(177, Short.MAX_VALUE)
                                        .addComponent(termsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addGroup(WelcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(disagreeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(agreeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(34, 34, 34)))
        );

        ControlPanel.setVisible(false);

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel3.setText("Software Downloader");

        resArea.setColumns(20);
        resArea.setRows(5);
        resPane.setViewportView(resArea);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        jLabel4.setText("Command Line Control");

        cmdArea.setColumns(20);
        cmdArea.setRows(5);
        cmdPane.setViewportView(cmdArea);

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        jLabel5.setText("Ressources:");

        exitBtn.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        exitBtn.setText("Exit");
        exitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitBtnActionPerformed(evt);
            }
        });

        messageArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messageAreaActionPerformed(evt);
            }
        });

        messageArea.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                changed();
            }
            public void changed() {
                if (messageArea.getText().equals("")){
                    sendBtn.setEnabled(false);
                }
                else {
                    sendBtn.setEnabled(true);
                }
        }});

        sendBtn.setFont(new java.awt.Font("Ubuntu", 0, 10)); // NOI18N
        sendBtn.setText("Send");
        sendBtn.setEnabled(false);
        sendBtn.setToolTipText("");
        sendBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ControlPanelLayout = new javax.swing.GroupLayout(ControlPanel);
        ControlPanel.setLayout(ControlPanelLayout);
        ControlPanelLayout.setHorizontalGroup(
                ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ControlPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(ControlPanelLayout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(ControlPanelLayout.createSequentialGroup()
                                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ControlPanelLayout.createSequentialGroup()
                                                                .addComponent(messageArea)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(sendBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jLabel4)
                                                        .addComponent(cmdPane, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(44, 44, 44)
                                                .addGap(18, 18, 18)
                                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(resPane, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel5)
                                                        .addComponent(exitBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(76, 76, 76))))
        );
        ControlPanelLayout.setVerticalGroup(
                ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ControlPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(resPane, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                                        .addComponent(cmdPane))
                                .addGap(18, 18, 18)
                                .addGroup(ControlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(messageArea, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(sendBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(exitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(132, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
                MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 725, Short.MAX_VALUE)
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MainPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(welcomePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addContainerGap()))
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MainPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(ControlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addContainerGap()))
        );
        MainPanelLayout.setVerticalGroup(
                MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 505, Short.MAX_VALUE)
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MainPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(welcomePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addContainerGap()))
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(MainPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(ControlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 725, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 505, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }

    private void DisagreeBtnActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }

    private void AgreeBtnActionPerformed(java.awt.event.ActionEvent evt) {
        sendMessage("yes");
        welcomePanel.setVisible(false);
        ControlPanel.setVisible(true);
    }

    private void exitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }

    private void messageAreaActionPerformed(java.awt.event.ActionEvent evt) {
        sendMessage(messageArea.getText());
        messageArea.setText("");
    }

    private void sendBtnActionPerformed(java.awt.event.ActionEvent evt) {
        sendMessage(messageArea.getText());
        messageArea.setText("");
    }

    // GUI end ***************************************************************

    //connect to server
    public void startRunning() {
        try {
            connectToServer();
            setupStreams();
            whileChatting();
        } catch (IOException e) {
            termsArea.setText("Cannot connect to the server");
            agreeBtn.setEnabled(false);
            disagreeBtn.setText("Exit");
            jLabel1.setText("Error");
        }
    }

    //connect to server
    private void connectToServer() throws IOException {
        showMessage("Attempting connection...");
        connection = new Socket(InetAddress.getByName(serverIP), 12345);
        showMessage("Connection Established! Connected to: " + connection.getInetAddress().getHostName());
    }

    //set up streams
    private void setupStreams() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        input = new ObjectInputStream(connection.getInputStream());
        showMessage("\n The streams are now set up! \n");
    }

    //while chatting with server
    private void whileChatting() throws IOException {
        do {
            try {
                message = (String) input.readObject();
                showMessage(message + "\n");
            } catch (ClassNotFoundException classNotFoundException) {
                showMessage("Unknown data received!");
            }
        } while (!message.equals("END\n"));
        closeConnection();
    }

    //close connection
    private void closeConnection() {
        try {
            output.close();
            input.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //send message to server
    void sendMessage(String message) {
        try {
            cmdArea.setText("");
            output.writeObject(message);
            output.flush();

        } catch (IOException ioException) {
            cmdArea.append("Connection lost with the server!.. Try to restart the program later...");
        }
    }

    //update chat window
    private void showMessage(String message) {
        if (message.startsWith("1-")) {
            SwingUtilities.invokeLater(() -> {
                resArea.setText(message);
            });
        } else if(message.startsWith("http")){
            download();
        }else if(message.startsWith("Here")){
            SwingUtilities.invokeLater(() -> {
                termsArea.setText(message);
            });
        }else {
            SwingUtilities.invokeLater(() -> {
                cmdArea.append(message);
            });
        }
    }

    //download the URL
    private void download(){
        try{
            messageArea.setEditable(false);
            URL website = new URL(message);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            String filename = message.substring(message.lastIndexOf("/"));
            FileOutputStream fos = new FileOutputStream(filename);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            messageArea.setEditable(true);
        }catch (IOException e){
            cmdArea.append("Cannot connect!! You have to run the program later..");
            messageArea.setEditable(false);
            sendBtn.setEnabled(false);
        }finally {
        }
    }
}
