
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class ClientOld extends JFrame {

    private JTextField userText;
    private JTextArea chatWindow;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String message = "";
    private String serverIP;
    private Socket connection;


    //constructor
    public ClientOld(String host) {
        serverIP = host;
        drawComponent();
    }

    public void drawComponent() {

        userText = new JTextField();

        chatWindow = new JTextArea();
        userText.setEditable(false);
        chatWindow.setHighlighter(null);

        userText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                sendMessage(e.getActionCommand());
                userText.setText("");
            }
        });

        add(userText, BorderLayout.NORTH);
        JScrollPane pane = new JScrollPane(chatWindow);
        pane.setBackground(Color.red);
        add(pane, BorderLayout.CENTER);

        setPreferredSize(new Dimension(500, 350));
        setVisible(true);
        pack();
    }

    //connect to server
    public void startRunning() {
        try {
            connectToServer();
            setupStreams();
            whileChatting();
        } catch (IOException e) {
            showMessage("\nSomething went wrong..");
        }
    }

    //connect to server
    private void connectToServer() throws IOException {
        showMessage("Attempting connection...");
        connection = new Socket(InetAddress.getByName(serverIP), 12345);
        showMessage("Connection Established! Connected to: " + connection.getInetAddress().getHostName());
    }

    //set up streams
    private void setupStreams() throws IOException {
        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush();
        input = new ObjectInputStream(connection.getInputStream());
        showMessage("\n The streams are now set up! \n");
    }

    //while chatting with server
    private void whileChatting() throws IOException {
        ableToType(true);
        do {
            try {
                message = (String) input.readObject();
                showMessage("\n" + message);
            } catch (ClassNotFoundException classNotFoundException) {
                showMessage("Unknown data received!");
            }
        } while (true);
    }

    //Close connection
    private void closeConnection() {
        showMessage("\n Closing the connection!");
        ableToType(false);
        try {
            output.close();
            input.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //send message to server
    private void sendMessage(String message) {
        try {
            chatWindow.setText("");
            output.writeObject(message);
            output.flush();
            showMessage("\n" + message);

        } catch (IOException ioException) {
            chatWindow.append("\n Oops! Something went wrong!");
        }
    }

    //update chat window
    private void showMessage(final String message) {
        if(message.contains("http")){
            try{
                System.out.println(1111);
                URL website = new URL(message);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                FileOutputStream fos = new FileOutputStream("/home/abdoo/Desktop/img_forest.jpg");
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            }catch (IOException e){

            }
        }else{
            SwingUtilities.invokeLater(() -> {
                chatWindow.append(message);
            });
        }
    }

    //allows user to type
    private void ableToType(final boolean tof) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                userText.setEditable(tof);
            }
        });
    }
}